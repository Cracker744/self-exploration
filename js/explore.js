﻿function l(msg) {
	_l('explore.js', msg);
}


var PROGRESS_TEMPLATE = 'Вопрос {{0}} из {{1}}';

(function () {
    'use strict';
    var SERVER_URI = '{{0}}//odin-dva.ru/explore/?{{1}}sid={{2}}&callback={{3}}', sessionId = '', protocol = 'http:';

    function insertScript(src) {
        var script = document.createElement('SCRIPT');
        script.src = src;
        document.getElementsByTagName('head')[0].appendChild(script);
    }
    var ServerApi = {
        "init": function (proto, sid) {
            protocol = proto;
            sessionId = sid;
        },
        "call": function (args, callback) {
            var _args = '', i = 0;
            var callbackName = 'jsonp' + Date.now();
            args['callback'] = callbackName;
	    for (i in args) {
                if (typeof (args[i]) !== 'function') {
                    _args += sformat('{{0}}={{1}}&', i, args[i]);
                }
            }
            var uri = sformat(SERVER_URI, protocol, _args, sessionId);
            window[callbackName] = callback;
            insertScript(uri);
        }
    };
    window.ServerApi = ServerApi;
}());

(function () {
    'use strict';
    var HISTORY_ADDED_EVENT = 'history',
        TESTS_ADDED_EVENT = 'tests',
		TEST_STARTED_EVENT = 'start',
        TEST_ENDED_EVENT = 'end',
        QUESTION_UPDATED_EVENT = 'question';
    var currentScore = 0,
        currentQuestion = 0,
        currentTest = {},
        events = {};

    function emitEvent(eventName, args) {
        var event = events[eventName], i = 0;
		if(typeof(event)!=='undefined') {
			for (; i < event.length; i++) {
				event[i](args);
			}
		}
    }

    function submitCurrentTest(callback) {
        ServerApi.call({
            "act": "submit",
            "score": currentScore,
            "test_id": currentTest.id
        }, function (result) {
            if (typeof(callback) !== 'undefined') {
                callback(result);
            }
        });
    }
    var App = {
        "on": function (eventName, listener) {
            if (typeof(events[eventName]) == 'undefined') {
                events[eventName] = [];
            }
            events[eventName].push(listener);
        },
        "tests": {},
        "history": {},
        "addHistory": function (history) {
            var i = 0;
			for (i = 0; i < history.length; i++) {
                self.history[history[i].id] = history[i];
            }
            emitEvent(HISTORY_ADDED_EVENT, history);
        },
        "addTests": function (tests) {
			l('adding '+ tests.length + ' tests...');
			var i = 0;
            for (i = 0; i < tests.length; i++) {
                self.tests[tests[i].id] = tests[i];
            }
            emitEvent(TESTS_ADDED_EVENT, tests);
        },
        "fetchTests": function () {
            ServerApi.call({
                "act": "tests"
            }, self.addTests);
        },
        "fetchHistory": function () {
            ServerApi.call({
                "act": "history"
            }, self.addHistory);
        },
        "startTest": function (testId) {
			l('starting test with id ' + testId);
            currentTest = self.tests[testId];
            currentScore = 0;
            currentQuestion = 0;
			emitEvent(TEST_STARTED_EVENT, {});
            emitEvent(QUESTION_UPDATED_EVENT, currentTest.questions[currentQuestion]);
        },
		"getQuestionNumber" : function() {
			return currentQuestion + 1;
		},
		"getTestLength" : function() {
			return currentTest.length;
		},
        "answerQuestion": function (answerId) {
            currentScore += currentTest.questions[currentQuestion].answers[answerId].score;
            currentQuestion += 1;
            if (currentTest.length <= currentQuestion) {
                submitCurrentTest(function (result) {
                    emitEvent(TEST_ENDED_EVENT, result);
                });
            } else {
                emitEvent(QUESTION_UPDATED_EVENT, currentTest.questions[currentQuestion]);
            }
        },
        "init": function (initData) {
            self.addTests(initData.tests);
            self.addHistory(initData.history);
            ServerApi.init(window.location.protocol, initData.sid);
        }
    };
    var self = App;
    window.App = App;
}());

function onDocumentReady() {
    ServerApi.init(window.location.protocol);
    App.on('tests', onTestsAdded);
    App.on('history', onHistoryAdded);
	App.on('question', onQuestionUpdated);
	App.on('start', onTestStarted);
    App.on('end', onTestEnded);
    App.init(initData);
	initBeautifiers();
}

function onTestsAdded(e) {
	l(e.length + ' tests were added');
	$('.tests').append(new EJS({"url":"/static/ejs/tests.ejs"}).render({"tests":e}));
	$('.test-info').off('click').on('click', function() {
		$(this).parent().next().slideToggle("slow");
	})
}

function onHistoryAdded(e) {
	l('history added');
}

function onTestStarted(e) {
	l('test started');
}

function onTestEnded(e) {
	l('test ended');
	$('.pre').find('span').text(e.pre);
	$('.main').find('h1').text(e.main);
	$('.description').find('p').text(e.description);
	goTo(3, false);
}

function onQuestionUpdated(e) {
	$('.question-text').find('p').html(e.text);
	var answers = e.answers,
		$answers = $('.answers').find('li');
	for(var i=0; i<answers.length; i++) {
		$answers.eq(i).show().find('span').html(answers[i].text);
	}
	for(; i<4; i++) {
		$answers.eq(i).hide();
	}
	var $progress = $('.progress');
	/* 880px - whole body, 440px - half */
	$progress.html(sformat(PROGRESS_TEMPLATE, App.getQuestionNumber(), App.getTestLength())).css('left', (((440-$progress.outerWidth()/2)/880)*100)+'%')
}


var gotoStack = [0];
 
function goTo(index, push) {
	var offset = 0;
	var $tabs = $('.content-tab');
	for(var i=0; i<index; i++) {
		offset -= $tabs.outerWidth();
	}
	if(typeof(push)=='undefined'||push) {
		gotoStack.push(index);
	}
	l('sliding to tab #'+index+' with offset = '+offset);
	$('.content').animate({'left':offset});
	if(gotoStack.length==1) {
		$('.back').hide();
	} else {
		$('.back').show();
	}
}

function goBack() {
	gotoStack.pop();
	goTo(gotoStack[gotoStack.length-1], false);
}


function initBeautifiers() {
	$('.back').on('click', goBack);
	$('.tests-button').on('click', function() {
		goTo(1);
	});
	$('.test-take').on('click', function() {
		var id = $(this).parent().attr('id');
		id = id.substring(id.indexOf('-') + 1);
		App.startTest(id)
		goTo(2);
	});
	$('#one').on('click', function() {
		App.answerQuestion(0);
	});
	
	$('#two').on('click', function() {
		App.answerQuestion(1);
	});
	
	$('#three').on('click', function() {
		App.answerQuestion(2);
	});
	
	$('#four').on('click', function() {
		App.answerQuestion(3);
	});
}



$(document).ready(onDocumentReady);
