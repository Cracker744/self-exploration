﻿function l(msg) {
	_l('editor.js', msg);
}
(function() {
	function l(msg) {
		_l('editor.js', msg);
	}
	function emitEvent(eventName, args) {
        var event = events[eventName], i = 0;
		if(typeof(event)!=='undefined') {
			for (; i < event.length; i++) {
				event[i](args);
			}
		}
    }
	function addIndices() {
		for(var i=0; i<currentTest.questions.length; i++) {
			currentTest.questions[i].id = i;
			for(var j=0; j<currentTest.questions[i].answers.length; j++) {
				currentTest.questions[i].answers[j].id = j;
				currentTest.questions[i].answers[j].qid = i;
			}
		}
	}
	function resetTest() {
		currentTest = EMPTY_TEST;
		currentTest.id = -1;
		currentTest.name = "";
		currentTest.length = 0;
		currentTest.description = "";
		currentTest.questions = [];
	}
	var EMPTY_TEST = {
			"id": -1,
			"name": "",
			"length": 0,
			"description": "",
			"questions": []
		},
		EMPTY_QUESTION = {
			"text" : "",
			"answers" : []
		},
		EMPTY_ANSWER = {
			"text" : "",
			"score" : 0
		},
		QUESTION_ADDED_EVENT = 'questionadded',
		ANSWER_ADDED_EVENT = 'answeradded',
		QUESTION_REMOVED_EVENT = 'questionremoved',
		ANSWER_REMOVED_EVENT = 'answerremoved',
		TEST_UPDATED_EVENT = 'testupdated',
		NAME_UPDATED_EVENT = 'nameupdated',
		DESCRIPTION_UPDATED_EVENT = 'descriptionupdated';
		
	var currentTest = {},
		events = {};
	var Editor = {
		"on" : function(eventName, listener) {
		    if (typeof(events[eventName]) == 'undefined') {
                events[eventName] = [];
            }
            events[eventName].push(listener);
		},
		"addQuestion" : function(text) {
			var q = {"text" : text, "answers" : []};
			currentTest.questions.push(q);
			q.id = currentTest.questions.length - 1;
			emitEvent(QUESTION_ADDED_EVENT, q);
		},
		"addAnswer" : function(qid, text, score) {
			var a = {"text" : text, "score" : score};
			currentTest.questions[qid].answers.push(a);
			a.id = currentTest.questions[qid].answers.length - 1; 
			a.qid = qid;
			emitEvent(ANSWER_ADDED_EVENT, a);
		},
		"removeQuestion" : function(qid) {
			currentTest.questions.splice(qid, 1);
			emitEvent(QUESTION_REMOVED_EVENT, qid);
		},
		"removeAnswer" : function(qid, aid) {
			currentTest.questions[qid].answers.splice(aid, 1);
			emitEvent(ANSWER_REMOVED_EVENT, aid);
		},
		"setName" : function(name) {
			currentTest.name = name;
		},
		"setDescription" : function(desc) {
			currentTest.description = desc;
		},
		"newTest" : function() {
			resetTest();
			emitEvent(TEST_UPDATED_EVENT, currentTest);
		},
		"openTest" : function(test) {
			currentTest = test;
			addIndices();
			emitEvent(TEST_UPDATED_EVENT, currentTest);
		},
		"getTestObject" : function() {
			return currentTest;
		},
		"saveTest" : function() {
			currentTest.length = currentTest.questions.length;
			if(currentTest.id == -1) {
				currentTest.id = null;
			}
			$.post('/explore/?act=save_test', {
				test : JSON.stringify(currentTest)
			}, function(data) {
				var response = JSON.parse(data);
				l(response);
			}) 
		},
		"addRule" : function(rule) {
			$.post('/explore/?act=add_rule', 
			{
				'rule' : JSON.stringify(rule)
			}, function(data) {
				var response = JSON.parse(data);
				l(response);
			});
		}
	};
	window.Editor = Editor;
})();
var CANONICAL_TEST = {
    "id": 123123,
    "name": "Тест на аутизм (Андерсон не прошёл)",
    "length": 1,
    "prohojdenii": 3,
    "date_added": "28.07.1995",
    "description": "Ну давай раздберем по частям тобою написанное)))))))))))",
    "questions": [{
        "text": "Как твои дела, дорогой мой товарищ?",
        "answers": [{
            "text": "Гусь свинье не товарищ",
            "score": 0
        }, {
            "text": "Подлива готова",
            "score": 1
        }, {
            "text": "Нормально",
            "score": 2
        }]
    }]
};
var CANONICAL_RESULT = {
    "id": 123123,
    "test_id": 123123,
    "timestamp": 1392480008291,
    "pre": "У тебя",
    "main": "АУТИЗМ",
    "description": "Обратись к специалисту\n, пока не слишком поздно. Подумай о\nсвоих близких!"
};
var CANONICAL_TESTS = [CANONICAL_TEST, CANONICAL_TEST, CANONICAL_TEST], CANONICAL_HISTORY = [CANONICAL_RESULT, CANONICAL_RESULT, CANONICAL_RESULT];


$(document).ready(function() {
	$('#add-q').on('click', function() {
		addQuestion();
	});
	$('#save-test').on('click', function() {
		Editor.saveTest();
	});
	$('#add-rule').on('click', function() {
		Editor.addRule({
			'tid' : $('#test_id').val(),
			'pre' : $('#pre').val(),
			'description' : $('#desc').val(),
			'main' : $('#main').val(),
			'min_score' : parseInt($('#min_score').val())
		});
	});
	$('#test-name').change(function() {
		Editor.setName($('#test-name').val());
	});
	$('textarea').change(function(){
		Editor.setDescription($('textarea').val());
	});
	$('#reload').on('click', function() {
		Editor.openTest(Editor.getTestObject());
	});
	$('#question-text').on('keydown', onQKeyDown);
	Editor.on('descriptionupdated', onDescriptionUpdated);
	Editor.on('nameupdated', onNameUpdated);
	Editor.on('testupdated', onTestUpdated);
	Editor.on('questionadded', onQuestionAdded);
	Editor.on('answeradded', onAnswerAdded);
	Editor.on('questionremoved', onQuestionRemoved);
	Editor.on('answerremoved', onAnswerRemoved);
	Editor.newTest();
	var tests = initData.tests;
	for(var i in tests) {
		if(typeof(tests[i])!='undefined') {
			$('#test-selector').append('<option value="' + i + '">ID: ' + tests[i].id + ', ' + tests[i].name + '</option>')
		}
	}
	$('#test-selector').change(function() {
		var id = $('#test-selector :selected').attr('value');
		if(id==-1) {
			Editor.newTest();
		} else {
			Editor.openTest(tests[id]);
		}
	});
});


function drawAnswer(a) {
	$('ol[data-qid='+a.qid+']').append('<li data-qid="'+a.qid+' data-aid="'+a.id+'""><span>'+a.text+', '+a.score+' баллов</span><button onclick="removeAnswer('+a.qid+', '+a.id+');">X</button></li>'); 
}
function drawQuestion(q) {
	$('#questions').append('<li><span>'+q.text+'</span><button onclick="removeQuestion('+q.id+');">X</button><ol data-qid="'+q.id+'"></ol><br><span>Текст:</span><input class="text" type="text" data-qid="'+q.id+'" onkeydown="onAKeyDown(event, '+q.id+');"></input><br><span>Количество баллов:</span><input type="text" class="score" data-qid="'+q.id+'" onkeydown="onAKeyDown(event, '+q.id+');"></input><button class="add-answer" data-qid="'+q.id+'" onclick="addAnswer('+q.id+')">Добавить ответ</button></li>');
}

function redrawAll(t) {
	$('h1').text('ID: ' + t.id)
	$('#questions').html('');
	$('#test-name').val(t.name);
	$('textarea').val(t.description);
	var questions = t.questions;
	for(var i=0; i<questions.length; i++) {
		drawQuestion(questions[i]);
		var answers = questions[i].answers;
		for(var j=0; j<answers.length; j++) {
			drawAnswer(answers[j]);
		}
	}

} 

function removeAnswer(qid, aid) {
	Editor.removeAnswer(qid, aid);
}

function removeQuestion(qid) {
	Editor.removeQuestion(qid);
}

function checkIfEnter(e) {
	return e.keyCode == 13;
}

function onAKeyDown(e, qid) {

	if(checkIfEnter(e)) {
		addAnswer(qid);
	}
}

function onQKeyDown(e) {
	if(checkIfEnter(e)) {
		addQuestion();
	}
}

function addAnswer(qid) {
	$text = $('.text[data-qid='+qid+']');
	$score = $('.score[data-qid='+qid+']');
	Editor.addAnswer(qid, $text.val(), parseInt($score.val()));
	$text.val('');
	$score.val('');
	$text.focus();
}

function addQuestion() {
	$input = $('#question-text');
	Editor.addQuestion($input.val());
	$input.val('');
	$input.focus();
}

function onTestUpdated(t) {
	l('test updated!');
	redrawAll(t);
}

function onAnswerAdded(e) {
	drawAnswer(e);
}

function onQuestionAdded(e) {
	drawQuestion(e);
}

function onNameUpdated(e) {
	$('#test-name').val(e);
}

function onDescriptionUpdated(e) {
	$('textarea').val(e);
}

function onQuestionRemoved(e) {
	redrawAll(Editor.getTestObject());
}

function onAnswerRemoved(e) {
	redrawAll(Editor.getTestObject());
}
